#ifndef DLINKEDLIST_H
#define DLINKEDLIST_H

// Custom linked list template class
// DListNode models a node in the list
// DLinkedList models the list
// DListIterator to iterate through the list

// Forward class declarations
template<class Datatype> class DListNode;
template<class Datatype> class DLinkedList;
template<class Datatype> class DListIterator;

// DListNode - Basic doubly linked list node
template<class Datatype>
class DListNode
{
private:
	// mData - The data stored in each node
	Datatype mData;

	// mNext - A pointer to the next node in the list
	DListNode<Datatype>* mNext;

	// mPrev - A pointer to the previous node in the list
	DListNode<Datatype>* mPrev;

public:
	// DListNode() - Constructor to make an empty node
	DListNode(): mNext(0), mPrev(0)
	{

	}

	// insertAfter() - Inserts an new node after the current node
	void insertAfter(Datatype data)
	{
		// New empty node
		DListNode<Datatype>* newnode = new DListNode<Datatype>;

		// Assign data to mData inside node
		newnode->mData = data;

		// Assign mPrev to be the previous node in the list
		newnode->mPrev = this;

		// Assign mNext to be the next node in the list
		newnode->mNext = mNext;
		
		// Update mPrev of next node in list if it exists
		if(mNext != 0)
			mNext->mPrev = newnode;

		// Update mNext to point to the new node just added
		mNext = newnode;
	}

	// getNext() - Returns a pointer to the node after this node
	DListNode<Datatype>* getNext()
	{
		return mNext;
	}

	// getPrev() - Returns a pointer to the node before this node
	DListNode<Datatype>* getPrev()
	{
		return mPrev;
	}

	// setPrev() - Sets mPrev to point to the specified node
	bool setPrev(DListNode<Datatype>* node)
	{
		if(this == node)
			return false;
		else if(mNext == node)
			return false;

		mPrev = node;

		return true;
	}

	// setNext() - Sets mNext to point to the specified node
	bool setNext(DListNode<Datatype>* node)
	{
		if(this == node)
			return false;
		else if(mPrev == node)
			return false;

		mNext = node;

		return true;
	}

	// getData() - Returns a reference to the data stored in the node
	Datatype& getData()
	{
		return mData;
	}
};

// DLinkedList - Container class for the list
template<class Datatype>
class DLinkedList
{
private:
	// mHead - Pointer to the first node of the list
	DListNode<Datatype>* mHead;

	// mTail - Pointer to the last node of the list
	DListNode<Datatype>* mTail;

	// mCount - Count of all nodes on the list
	int mCount;


public:
	// DLinkedList() - Constructor for the node container list
	DLinkedList(): mHead(0), mTail(0), mCount(0)
	{
		
	}

	// ~SLinkedList() - Deconstructor of the list
	~DLinkedList()
	{
		// Temp node pointers to iterate through the list
		DListNode<Datatype>* itr = mHead;
		DListNode<Datatype>* next;

		// While itr is a valid node
		while(itr != 0)
		{
			// Set next to the next node in the list
			next = itr->getNext();
			
			// Delete the current node
			delete itr;

			// Make the current node the next node
			itr = next;
		}
	}

	// getCount() - Returns the number of nodes in the list
	int getCount()
	{
		return mCount;
	}

	// getHead() - Returns a pointer to the head of the list
	DListNode<Datatype>* getHead()
	{
		return mHead;
	}

	// getTail() - Returns a pointer to the tail of the list
	DListNode<Datatype>* getTail()
	{
		return mTail;
	}

	// append() - Adds a new node to the end of the list with data
	void append(Datatype data)
	{
		// If the list is empty
		if(mHead == 0)
		{
			// Create a new node that will be the head and tail of the list
			mHead = mTail = new DListNode<Datatype>;

			// Set the data of the node
			mHead->getData() = data;
		}
		else
		{
			// Otherwise if the list isn't empty, call the insertAfter() function of the tail
			mTail->insertAfter(data);

			// Set the new tail to the node just added
			mTail = mTail->getNext();
		}

		// Increment mCount
		++mCount;
	}

	// prepend() - Adds a new node to the start of the list with data
	void prepend(Datatype data)
	{
		// If the list is empty
		if(mHead == 0)
		{
			// Create a new node that will be the head and tail of the list
			mHead = mTail = new DListNode<Datatype>;

			// Set the data of the node
			mHead->getData() = data;
		}
		else
		{
			// Create the new node to add
			DListNode<Datatype>* newnode = new DListNode<Datatype>;

			// Set the data of the new node
			newnode->getData() = data;

			// Update the mPrev of the head to point to the new node
			mHead->setPrev(newnode);

			// Sets mNext of the new node to point to the head
			newnode->setNext(mHead);

			// Set mHead to point to the new node
			mHead = newnode;
		}

		// Increment mCount
		++mCount;
	}

	// insert() - Inserts a new node with the data passed in after the given iterator if the iterator is valid
	// Otherwise if iterator is not valid goes to the end
	void insert(DListIterator<Datatype>& iter, Datatype data)
	{
		// If the iterator is not of this list, return
		if(iter.getList() != this)
		{
			return;
		}
		
		// If iterator node is valid
		if(iter.getNode() != 0)
		{
			// Insert the data in a new node after the iterator
			iter.getNode()->insertAfter(data);

			// If iterator is the tail
			if(iter.getNode() == mTail)
			{
				// Update the tail node
				mTail = iter.getNode()->getNext();
			}

			// Increment mCount
			++mCount;
		}
		else
		{
			// If iterator not valid append at end
			append(data);
		}
	}

	// remove() - Removes the node that the iterator points to
	void remove(DListIterator<Datatype>& iter)
	{
		// If the iterator is not of this list
		if(iter.getList() != this)
		{
			// Return
			return;
		}

		// If the iterator node is not valid
		if(!iter.isValid())
		{
			// Return
			return;
		}

		// Create a new node pointing at the head of the list
		DListNode<Datatype>* node = mHead;

		// If the iterator is pointing at the head of the list
		if(iter.getNode() == mHead)
		{
			// Move the iterator forward
			iter.next();

			// Remove the head of the list
			removeHead();
		}
		else if(iter.getNode() == mTail)
		{
			// Move the iterator forward
			iter.next();

			// Remove the tail of the list
			removeTail();
		}
		else
		{
			// While the next node doesn't point to the iterator node
			while(node->getNext() != iter.getNode())
			{
				// Point node to the next node
				node = node->getNext();
			}

			// Move the iterator forward
			iter.next();

			// If the node after node is the tail
			if(node->getNext() == mTail)
			{	
				// Set the tail to point at node
				mTail = node;
			}

			// Delete the node
			delete node->getNext();
			
			// Set the next node to be the iterator node (Link the hole in the list)
			node->setNext(iter.getNode());

			// Set mPrev of the iter node to be the node before it (Patch the hole fully)
			iter.getNode()->setPrev(node);

			// Decrement mCount
			--mCount;
		}
	}

	// removeHead() - Removes the head of the list
	void removeHead()
	{
		// Create an empty node
		DListNode<Datatype>* node = 0;
		
		// If the list isn't empty
		if(mHead != 0)
		{
			// Set node to be the second node in the list
			node = mHead->getNext();

			// Delete the head
			delete mHead;

			// Set the head to point to the new node
			mHead = node;

			// If head is null then list is empty
			if(mHead == 0)
			{
				// Update mTail to account for this
				mTail = 0;
			}

			// Decrement mCount
			--mCount;
		}
	}

	// removeTail() - Removes the tail of the list
	void removeTail()
	{
		// Create a node pointing at the head
		DListNode<Datatype>* node = mHead;
		
		// If list is not empty
		if(mHead != 0)
		{
			// If the head and tail are the same node (1 element in list)
			if(mHead == mTail)
			{
				// Delete the head
				delete mHead;

				// Set both mHead and mTail to point to 0
				mHead = mTail = 0;

			}
			else
			{
				// While the next node isn't the last element
				while(node->getNext() != mTail)
				{
					// Point node at the next node in the list
					node = node->getNext();
				}

				// Point mTail at the second last node in the list
				mTail = node;

				// Delete the last node in the list
				delete mTail->getNext();

				// Update the mNext of the new mTail to be null
				mTail->setNext(0);
			}

			// Decrement mCount
			--mCount;
		}
	}

	// getIter() - Returns an iterator pointing to the head of the list
	DListIterator<Datatype> getIter()
	{
		return DListIterator<Datatype>(this, mHead);
	}

	// size() - Returns the number of nodes in the list
	int size()
	{
		return mCount;
	}

	// saveToDisk() - Saves the linked list to disk 
	bool saveToDisk(char* filename)
	{
		FILE* outfile = 0;
		DListNode<Datatype>* itr = mHead;

		outfile = fopen(filename, "wb");

		if(outfile == 0)
		{
			return false;
		}

		fwrite(&mCount, sizeof(mCount), 1, outfile);

		while(itr != 0)
		{
			fwrite(&(itr->getData()), sizeof(Datatype),1, outfile);
			itr = itr->getNext();
		}

		fclose(outfile);

		return true;
	}

	// readFromDisk() - Reads the list in from file on disk
	bool readFromDisk(char* filename)
	{
		FILE* infile = 0;
		Datatype buffer;
		int count = 0;

		infile = fopen(filename, "rb");

		if(infile == 0)
		{
			return false;
		}

		fread(&count, sizeof(int), 1, infile);

		while(count != 0)
		{
			fread(&buffer, sizeof(Datatype), 1, infile);
			append(buffer);
			--count;
		}

		fclose(infile);

		return true;
	}
};



// DListIterator - Iterator for the DLinkedList
template <class Datatype>
class DListIterator
{
private:
	// mList - The list the iterator belongs to
	DLinkedList<Datatype>* mList;

	// mNode - The current node the iterator points to
	DListNode<Datatype>* mNode;

public:
	// DListIterator() - Constructor for the iterator of a list pointing at a node
	DListIterator(DLinkedList<Datatype>* list, DListNode<Datatype>* node)
	{
		mList = list;
		mNode = node;
	}

	// DListIterator() - Default constructor
	DListIterator(): mNode(0)
	{
		
	}

	// start() - Points the iterator node at the head of the list
	void start()
	{
		// If list not null
		if(mList != 0)
		{
			// Point iterator node to the head of the list
			mNode = mList->getHead();
		}
	}

	// next() - Points the iterator node to the next node in the list
	void next()
	{
		// If iterator node not null
		if(mNode != 0)
		{
			// Point iterator node to the next node in the list
			mNode = mNode->getNext();
		}
	}

	// prev() - Points the iterator node to the previous node in the list
	void prev()
	{
		// If iterator node not null
		if(mNode != 0)
		{
			// Point iterator node to the previous node in the list
			mNode = mNode->getPrev();
		}
	}

	// isValid() - Returns if the current node is valid
	bool isValid()
	{
		return (mNode != 0);
	}

	// getNode() - Returns a pointer to the node the iterator is pointing at
	DListNode<Datatype>* getNode()
	{
		return mNode;
	}

	// getList() - Returns a pointer to the list the iterator belongs to
	DLinkedList<Datatype>* getList()
	{
		return mList;
	}

	// getData() - Returns the data stored at the current node
	Datatype& getData()
	{
		return mNode->getData();
	}
};

#endif