// Source.cpp - Source file for the player database

// Includes
#include "DLinkedList.h"
#include "Player.h"
#include <iostream>
#include <regex>
#include <vector>
#include <sstream>
#define _CRTDBG_MAP_ALLOC // For memory leaks

// Usings
using std::cout;
using std::endl;
using std::cin;
using std::string;
using std::regex;
using std::smatch;
using std::regex_search;
using std::vector;
using std::stringstream;

// Functions
int functionMenu(DLinkedList<Player>& list);
void printMenu();
void printList(DLinkedList<Player>& list);
void insertMenu(DLinkedList<Player>& list);
void deleteMenu(DLinkedList<Player>& list);
void searchMenu(DLinkedList<Player>& list);
vector<Player> searchDatabase(string field, string value, DLinkedList<Player>& list);
void removeDuplicates(DLinkedList<Player>& list);
void findAverage(DLinkedList<Player>& list);

// Main
int main()
{
	#if defined(DEBUG) | defined(_DEBUG)
		_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	#endif

	// Main list for the players
	DLinkedList<Player> playerList;

	// Main menu for the program
	functionMenu(playerList);

	// Windows CMD command for "Press any button to continue..."
	system("pause");

	return 1;
}

// functionMenu() - Menu to select which operation to carry out
int functionMenu(DLinkedList<Player>& list)
{
	// Operation - An enum to hold all of the choices for the menu
	enum Operation { Insert = 1, Delete, Search, Average, RemoveDuplicates, Print, Quit };
	
	// choice - Variable to store the user's choice
	int choice = 0;

	// while the user has not decided to quit the menu
	while(choice != Quit)
	{
		printMenu();

		// Take in the user's choice
		cin >> choice;

		// Clear the console (Windows exclusive)
		system("cls");

		// Carry out function depending on user's choice
		if(choice == Insert)
			insertMenu(list);
		else if(choice == Delete)
			deleteMenu(list);
		else if(choice == Search)
			searchMenu(list);
		else if(choice == RemoveDuplicates)
			removeDuplicates(list);
		else if(choice == Average)
			findAverage(list);
		else if(choice == Print)
			printList(list);
		else if(choice > Quit || choice < Insert)
			cout << "Not a valid option" << endl;
	}

	cout << "Exiting" << endl;

	return 0;
}

// printMenu() - Prints the main program menu
void printMenu()
{
	// Print the menu
	cout << "Welcome to the database menu!" << endl;
	cout << "Press 1 to insert a new record at a particular position" << endl;
	cout << "Press 2 to delete a record from a particular position" << endl;
	cout << "Press 3 to search the database and print results" << endl;
	cout << "Press 4 to find the average experience points of players at a particular level" << endl;
	cout << "Press 5 to find and remove all duplicate entries" << endl;
	cout << "Press 6 to print the database" << endl;
	cout << "Press 7 to quit" << endl;
	cout << "> ";
}

// printList() - Prints the contents of the player list
void printList(DLinkedList<Player>& list)
{
	// iter - iterator pointing at mHead
	DListIterator<Player> iter = list.getIter();
	
	// while iter is valid
	while(iter.isValid())
	{
		// Print current node's player
		iter.getData().print();

		// Move node ahead
		iter.next();
	}
}

// insertMenu() -  The menu for inserting a new record into the list
void insertMenu(DLinkedList<Player>& list)
{
	// end - The node count of the list
	int end = list.getCount();

	// Position - enum for where to insert record
	enum Position { Start };

	// Variables for new Player record
	string fName, lName;
	int level, exp;

	// pos - To hold user's choice for insertion position 
	int pos = -1;

	cout << "Insert a new record" << endl;

	// while position not valid (less than 0)
	while(pos < 0)
	{
		// Ask for position and store input in pos
		cout << "At what position should the record be inserted after (0 for start)" << endl;
		cout << "> ";
		cin >> pos;
	}

	// Take in values for new Player record
	cout << "Enter the player's first name" << endl;
	cout << "> ";
	cin >> fName;

	cout << "Enter the player's last name" << endl;
	cout << "> ";
	cin >> lName;

	cout << "Enter the player's level" << endl;
	cout << "> ";
	cin >> level;

	cout << "Enter the player's experience" << endl;
	cout << "> ";
	cin >> exp;

	// Add Player to list depending on choice of position
	if(pos == Start)
		list.prepend(Player(fName, lName, level, exp));
	else if(pos >= end)
		list.append(Player(fName, lName, level, exp));
	else
	{
		DListIterator<Player> iter = list.getIter();
	
		// Move iterator forward to specified position
		for(int i = 1; i < pos; ++i)
			iter.next();

		list.insert(iter, Player(fName, lName, level, exp));
	}
}

// deleteMenu() - Menu for deleting a record from the list
void deleteMenu(DLinkedList<Player>& list)
{
	// end - The node count of the list
	int end = list.getCount();

	// Position - enum for which record to delete
	enum Position { Start };

	// pos - To hold user's choice for deletion position 
	int pos = -1;

	cout << "Delete an existing record" << endl;

	// while position not valid (less than 0)
	while(pos < 0)
	{
		// Ask for position and store input in pos
		cout << "Enter the record number to delete" << endl;
		cout << "> ";
		cin >> pos;
	}

	// Delete record from list
	if(pos == Start)
		list.removeHead();
	else if(pos >= end)
		list.removeTail();
	else
	{
		DListIterator<Player> iter = list.getIter();
	
		// Move iterator forward to specified position
		for(int i = 1; i < pos; ++i)
			iter.next();

		list.remove(iter);
	}
}

// searchMenu() - Function to display the search menu
void searchMenu(DLinkedList<Player>& list)
{
	string field, value;

	cout << "Search Menu" << endl;
	cout << "Fields: [firstname, lastname, level, experience]" << endl;
	cout << "Values: ['string', number, number-number]" << endl;
	cout << "Please enter a field to search for" << endl;
	cin >> field;

	cout << "Please enter a value to search for" << endl;
	cin >> value;

	searchDatabase(field, value, list);
}

// searchDatabase() - Function to perform the search of the database
vector<Player> searchDatabase(string field, string value, DLinkedList<Player>& list)
{
	// iter - Iterator to search through the list
	DListIterator<Player> iter = list.getIter();

	// vec - Vector to store results of the search
	vector<Player> vec;

	// If field to search for is firstname/lastname/level/experience	
	// Iterate through list and compare fields for matches
	// If matches, add to vector
	if(field == "firstname")
	{
		while(iter.isValid())
		{
			if(iter.getData().getFirstName() == value)
			{
				vec.push_back(iter.getData());
				iter.next();
			}
			else
			{
				iter.next();
			}
		}
	}
	else if(field == "lastname")
	{
		while(iter.isValid())
		{
			if(iter.getData().getLastName() == value)
			{
				vec.push_back(iter.getData());
				iter.next();
			}
			else
			{
				iter.next();
			}
		}
	}
	else if(field == "level")
	{
		// smatch - Stores regex results
		smatch sm;

		// convertOne - To store the first digit of the search value
		string convertOne;

		// convertTwo - To store the second digit of the search value
		string convertTwo;
		
		// If the input matches the format [digits-digits]
		if(regex_match (value, sm, regex("(\\d*)-(\\d*)")))
		{
			// Set variables to be the first and second digit
			convertOne = sm[1];
			convertTwo = sm[2];
		}
		else if(regex_match (value, sm, regex("(\\d*)")))
		{
			// Otherwise if the input matches the format [digit]
			// Set variable to match digit
			convertOne = sm[0];
		}
		else
		{
			// Otherwise print error and return empty vector of results
			cout << "Value not valid." << endl;
			return vec;
		}

		// String streams for converting from string to int
		stringstream convertFirst(convertOne);
		stringstream convertSecond(convertTwo);

		// ints to hold levels to search
		int first, second;

		// If the int is not read from the stringstream (i.e failure)
		// Set the variable to 0
		if ( !(convertFirst >> first) )
			first = 0;

		if ( !(convertSecond >> second) )
			second = 0;

		// If the second digit is > 0, assume user searching in range [lo-hi]
		if(convertSecond > 0)
		{
			// Iterate and compare
			while(iter.isValid())
			{
				if(iter.getData().getLevel() >= first && iter.getData().getLevel() <= second)
				{
					vec.push_back(iter.getData());
					iter.next();
				}
				else
				{
					iter.next();
				}
			}
		}
		else
		{
			// Otherwise assume user searching via single digit
			while(iter.isValid())
			{
				if(iter.getData().getLevel() == first)
				{
					vec.push_back(iter.getData());
					iter.next();
				}
				else
				{
					iter.next();
				}
			}
		}
	}
	else if(field == "experience")
	{
		smatch sm;
		string convertOne;
		string convertTwo;
		
		if(regex_match (value, sm, regex("(\\d*)-(\\d*)")))
		{
			convertOne = sm[1];
			convertTwo = sm[2];
		}
		else if(regex_match (value, sm, regex("(\\d*)")))
		{
			convertOne = sm[0];
		}
		else
		{
			cout << "Value not valid." << endl;
			return vec;
		}

		stringstream convertFirst(convertOne);
		stringstream convertSecond(convertTwo);

		int first, second;

		if ( !(convertFirst >> first) )
			first = 0;

		if ( !(convertSecond >> second) )
			second = 0;

		if(convertSecond > 0)
		{
			while(iter.isValid())
			{
				if(iter.getData().getExp() >= first && iter.getData().getExp() <= second)
				{
					vec.push_back(iter.getData());
					iter.next();
				}
				else
				{
					iter.next();
				}
			}
		}
		else
		{
			while(iter.isValid())
			{
				if(iter.getData().getExp() == first)
				{
					vec.push_back(iter.getData());
					iter.next();
				}
				else
				{
					iter.next();
				}
			}
		}
	}
	else
	{
		cout << "Field not valid" << endl;
	}

	// Print out vector of results
	for (vector<Player>::iterator it = vec.begin(); it != vec.end(); ++it)
		it->print();

	// Return the vector of results
	return vec;
}

// removeDuplicates() - Removes all duplicates from the list
void removeDuplicates(DLinkedList<Player>& list)
{
	if(list.getCount() == 0)
		return;

	// iter - Iterator to iterate through the list
	DListIterator<Player> iter = list.getIter();
	DListIterator<Player> compare = list.getIter();

	// Have compare start at second element
	compare.next();

	// Max number of iterations required is size of list
	int count = list.getCount();

	// Outer loop - Updates the node being checked against
	for(int k = 0; k < count; ++k)
	{
		if(iter.isValid())
		{
			// Inner loop - Updates the node being compared
			for(int i = 0; i < count; ++i)
			{
				if(compare.isValid())
				{
					// If the Players stored are equal, remove it
					if(iter.getData().equals(compare.getData()))
					{
						list.remove(compare);
					}
					else
					{
						compare.next();
					}
				}
			}
		}
		iter.next();

		// Reset iterator for next loop
		compare = iter;
		compare.next();
	}
}

// findAverage() - Prints out the average experience of all players on a certain level or level range
void findAverage(DLinkedList<Player>& list)
{
	// Vector of players to iterate through
	vector<Player> players;
	
	// Level range to search by
	string search;

	// Get user input
	cout << "What level would you like to average the experience of? [number | low-high]" << endl;
	cin >> search;

	// Retrieve vector via search function
	players = searchDatabase("level", search, list);

	// Variable for average
	double average = 0;

	// If there are results, calculate average
	if(players.size() > 0)
	{
		for(int i = 0; i <  players.size(); ++i)
			average = average + players[i].getExp();

		cout << "Average of all players level " << search << " is " << (average/players.size()) << endl;
	}
	else
	{
		// Otherwise error
		cout << "No players found that match search." << endl;
	}
}