// Implementation of the Player class

// Includes
#include "Player.h"

// Usings
using std::string;

// Player() - Default constructor
Player::Player(): mFirstName(""), mLastName(""), mLevel(-1), mExperience(-1)
{

}

// Player(args) - Player constructor
Player::Player(const std::string& firstName, const std::string& lastName, int level, int exp)
{
	mFirstName = firstName;
	mLastName = lastName;
	mLevel = level;
	mExperience = exp;
}

// getExp() - Returns the experience of the player
int Player::getExp()
{
	return mExperience;
}

// getLevel() - Returns the level of the player
int Player::getLevel()
{
	return mLevel;
}

// getFirstName() - Returns the first name of the player
string Player::getFirstName()
{
	return mFirstName;
}

// getLastName() - Returns the last name of the player
string Player::getLastName()
{
	return mLastName;
}

// print() - Prints a player's details
void Player::print()
{
	std::cout << "Player name:\t\t" << mFirstName << " " << mLastName << std::endl;
	std::cout << "Player level:\t\t" << mLevel << std::endl;
	std::cout << "Player experience:\t" << mExperience << std::endl;
	std::cout << std::endl;
}

// equals() - Compares Players by firstname/lastname basis
bool Player::equals(Player comp)
{
	// If both firstnames and lastnames are the same, consider the Players the same
	if(comp.getFirstName() == mFirstName && comp.getLastName() == mLastName)
		return true;

	return false;
}