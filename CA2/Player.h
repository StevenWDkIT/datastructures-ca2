#ifndef PLAYER_H
#define PLAYER_H
// Header file for the Player class

// Includes
#include <string>
#include <iostream>

class Player
{
private:
	// Member variables
	std::string mFirstName;
	std::string mLastName;
	int mLevel;
	int mExperience;

public:
	// Constructors
	Player();
	Player(const std::string& firstName, const std::string& lastName, int level, int exp);

	// Functions
	std::string getFirstName();
	std::string getLastName();
	int getLevel();
	int getExp();
	void print();
	bool equals(Player comp);
};

#endif